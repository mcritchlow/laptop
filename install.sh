#!/bin/bash

# Override as needed
USER="mcritchlow"
# alt video driver options: xf86-video-nouveau
DRIVERS="xf86-video-intel libva-intel-driver"

# capture term for prompts
term=$(tty)
password_prompt(){
  local context="${1}"; shift
  local password
  local password_2
  echo "Enter the $context password: " > "$term"
  read -rs password
  echo "Enter the $context password(again): " > "$term"
  read -rs password_2
  while [ "$password" != "$password_2" ]
  do
    echo "Enter the $context password: " > "$term"
    read -rs password
    echo "Enter the $context password(again): " > "$term"
    read -rs password_2
  done
  echo "$password"
}

function join_result { local IFS=","; echo "$*"; }

setup(){
  pacman -Syy
  pacman -S --noconfirm --needed reflector parted || (echo "Error at script start: Are you sure you have an internet connection?" && exit)
  reflector --verbose -l 15 --sort rate --save /etc/pacman.d/mirrorlist

  timedatectl set-ntp true

  parted -s /dev/sda \
    mklabel msdos \
    mkpart primary ext4 1 250M \
    mkpart primary ext4 250M 100% \
    set 1 boot on

  mkfs.ext4 /dev/sda1
  mkfs.ext4 /dev/sda2
  mount /dev/sda2 /mnt
  mkdir /mnt/boot
  mount /dev/sda1 /mnt/boot

  echo "Pacstrapping!.."
  pacstrap_packages='base ansible python2 git sudo '
  pacstrap_packages+=$DRIVERS
  pacstrap /mnt $pacstrap_packages

  genfstab -U /mnt >> /mnt/etc/fstab

  echo 'Chrooting into installed system to continue setup...'
  cp $0 /mnt/install.sh
  arch-chroot /mnt ./install.sh chroot

  echo 'Unmounting filesystems'
  umount /mnt/boot
  umount /mnt
  echo 'Done! Reboot system.'
}

chroot_setup(){
  pacman -R --noconfirm linux

  ROOT_PASSWORD=$(password_prompt "root")
  echo "root:$ROOT_PASSWORD" | chpasswd

  echo "Creating user now.."
  USER_PASSWORD=$(password_prompt "user")
  useradd -m -g wheel -G systemd-journal,lp -s /bin/bash "$USER"
  echo "$USER:$USER_PASSWORD" | chpasswd


  VAULT_PASSWORD=$(password_prompt "ansible vault")
  echo "Install work packages? (yes/no)"
  read -r work_packages

  tags_to_skip=''
  if [[ ! "$work_packages" =~ ^y ]]; then
    tags_to_skip+='work'
  fi
  tags_to_skip=$(join_result $tags_to_skip)

  echo "Running ansible provisioning scripts now.."
  git clone https://git.sr.ht/~mcritchlow/laptop.git /tmp/laptop
  cd /tmp/laptop || exit
  git checkout arch-dwm
  cp files/sudoers /etc/sudoers
  echo "$VAULT_PASSWORD" > .vault_pass

  if [[ "$tags_to_skip" ]]; then
    echo "Running ansible, skipping tags $tags_to_skip"
    sudo -u "$USER" ansible-playbook main.yml --vault-password-file .vault_pass --skip-tags "$tags_to_skip" --extra-vars "bundler_cpus=$(nproc)"
  else
    echo "Running ansible, installing all packages"
    sudo -u "$USER" ansible-playbook main.yml --vault-password-file .vault_pass --extra-vars "bundler_cpus=$(nproc)"
  fi
  echo "Installing grub.."
  pacman --noconfirm --needed -S grub
  grub-install --target=i386-pc /dev/sda
  grub-mkconfig -o /boot/grub/grub.cfg
}

if [ "$1" == "chroot" ]; then
  chroot_setup
else
  setup
fi


