#!/bin/bash

# Override as needed
USER="mcritchlow"
# alt video driver options: xf86-video-nouveau
DRIVERS="xf86-video-intel libva-intel-driver"
# hostname (uncomment and adjust as needed)
# HOST=arch4k
HOST="arch"
# Replace with higher (32) for hidpi
VCONSOLE_FONT="ter-v16n"

# capture term for prompts
term=$(tty)
password_prompt(){
  local context="${1}"; shift
  local password
  local password_2
  echo "Enter the $context password: " > "$term"
  read -rs password
  echo "Enter the $context password(again): " > "$term"
  read -rs password_2
  while [ "$password" != "$password_2" ]
  do
    echo "Enter the $context password: " > "$term"
    read -rs password
    echo "Enter the $context password(again): " > "$term"
    read -rs password_2
  done
  echo "$password"
}

get_uuid() { blkid -s UUID -o value "$1"; }

legacy_partition(){
  device="$1"
  # partition device
  parted -s $device \
    mklabel msdos \
    mkpart primary ext4 1 250M \
    mkpart primary ext4 250M 100% \
    set 1 boot on

  part_boot="$(ls ${device}* | grep -E "^${device}p?1$")"
  part_root="$(ls ${device}* | grep -E "^${device}p?2$")"

  cryptsetup --cipher aes-xts-plain64 \
            --key-size 512 \
            --hash sha512 \
            --iter-time 3000 \
            --type luks2 \
            --use-random luksFormat $part_root

  cryptsetup open $part_root cryptroot
  mkfs.ext4 /dev/mapper/cryptroot
  mount /dev/mapper/cryptroot /mnt

  mkfs.ext4 $part_boot
  mkdir /mnt/boot
  mount $part_boot /mnt/boot
}

efi_partition(){
  device="$1"
  # partition device
  parted -s $device \
    mklabel gpt \
    mkpart primary fat32 1MiB 551MiB \
    mkpart primary ext4 551MiB 100% \
    set 1 esp on

  part_boot="$(ls ${device}* | grep -E "^${device}p?1$")"
  part_root="$(ls ${device}* | grep -E "^${device}p?2$")"

  cryptsetup --cipher aes-xts-plain64 \
            --key-size 512 \
            --hash sha512 \
            --iter-time 3000 \
            --type luks2 \
            --use-random luksFormat $part_root

  cryptsetup open $part_root cryptroot
  mkfs.ext4 /dev/mapper/cryptroot
  mount /dev/mapper/cryptroot /mnt

  mkfs.fat -F32 $part_boot
  mkdir /mnt/boot
  mount $part_boot /mnt/boot
}

setup(){
  ### User prompts
  devicelist=$(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
  device=$(dialog --stdout --menu "Select installation disk" 0 0 0 ${devicelist}) || exit 1
  clear

  echo "Securely wipe ${device}? (yes/no)"
  read -r wipe_device

  timedatectl set-ntp true

  # securely wipe device
  if [[ "$wipe_device" =~ ^y ]]; then
    part_root="$(ls ${device}* | grep -E "^${device}p?2$")"
    cryptsetup open --type plain -d /dev/urandom "$part_root" to_be_wiped
    dd if=/dev/zero of=/dev/mapper/to_be_wiped status=progress bs=1M
    cryptsetup close to_be_wiped
  fi

  # determine boot mode
  boot_mode="legacy"
  if [ -d /sys/firmware/efi/efivars ]; then
    boot_mode="efi"
  fi

  # setup partioning scheme
  if [ "$boot_mode" == "legacy" ]; then
    echo 'Setting up legacy partitioning scheme'
    legacy_partition "$device"
  else
    echo 'Setting up EFI partitioning scheme'
    efi_partition "$device"
  fi

  pacstrap /mnt reflector
  reflector --verbose --country 'United States' --latest 25 --age 24 --sort rate --save /etc/pacman.d/mirrorlist

  echo "Pacstrapping!.."
  pacstrap_packages='base ansible python2 git sudo zsh '
  pacstrap_packages+=$DRIVERS
  pacstrap /mnt $pacstrap_packages

  genfstab -U /mnt >> /mnt/etc/fstab

  echo 'Chrooting into installed system to continue setup...'
  cp $0 /mnt/install-encrypt.sh
  arch-chroot /mnt ./install-encrypt.sh chroot "$device" "$boot_mode"

  echo 'Unmounting filesystems'
  umount /mnt/boot
  umount /mnt
  cryptsetup close cryptroot
  echo 'Done! Reboot system.'
}

chroot_setup(){
  device="$1"
  boot_mode="$2"

  if [ "$boot_mode" == "efi" ]; then
    echo "Installing systemd-boot config"
    bootctl --path=/boot install
  fi

  root_password=$(password_prompt "root")
  user_password=$(password_prompt "user")
  # ansible prompts
  vault_password=$(password_prompt "ansible vault")
  echo "Install work packages? (yes/no)"
  read -r work_packages

  echo "root:$root_password" | chpasswd
  useradd -m -g wheel -G systemd-journal,lp -s /bin/zsh "$USER"
  echo "$USER:$user_password" | chpasswd

  skip_work_packages=false
  if [[ ! "$work_packages" =~ ^y ]]; then
    skip_work_packages=true
  fi

  part_root="$(ls ${device}* | grep -E "^${device}p?2$")"
  crypt_device_id=$(get_uuid "$part_root")

  echo "Running ansible provisioning scripts now.."
  git clone https://git.sr.ht/~mcritchlow/laptop.git /tmp/laptop
  cd /tmp/laptop || exit
  git checkout master
  cp files/sudoers /etc/sudoers
  echo "$vault_password" > .vault_pass

  if [ "$skip_work_packages" = true ]; then
    echo "Running ansible, skipping work packages"
    sudo -u "$USER" ansible-playbook main.yml --vault-password-file .vault_pass \
      --skip-tags "work" \
      --extra-vars "bundler_cpus=$(nproc) crypt_device_id=$crypt_device_id boot_mode=$boot_mode laptop_host=$HOST laptop_console_font=$VCONSOLE_FONT"
  else
    echo "Running ansible, installing all packages"
    sudo -u "$USER" ansible-playbook main.yml --vault-password-file .vault_pass \
      --extra-vars "bundler_cpus=$(nproc) crypt_device_id=$crypt_device_id boot_mode=$boot_mode laptop_host=$HOST laptop_console_font=$VCONSOLE_FONT"
  fi

  if [ "$boot_mode" == "legacy" ]; then
    echo "Installing grub.."
    grub-install --target=i386-pc "$device"
    grub-mkconfig -o /boot/grub/grub.cfg
  fi
}

# do teh things
if [ "$1" == "chroot" ]; then
  chroot_setup "$2" "$3"
else
  setup
fi
