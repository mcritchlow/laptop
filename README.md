# Laptop

A set of ansible playbook for installing my dotfiles and configuring my Arch
system. This is wrapped with a bash script for handling some pre-chroot and
chroot tasks. The shortened git.io link below is for the encrypted install
script. It does a very simple partition scheme with root and boot partitions
using Legacy boot. I may add an UEFI version in the future.

My scripts also load my [dotfiles](https://git.sr.ht/~mcritchlow/dotfiles) as part of the install.

## Requirements
1. Create and boot an Arch ISO
1. `wget https://git.sr.ht/%7Emcritchlow/laptop/blob/master/install-encrypt.sh`
1. `chmod +x install-encrypt.sh`
1. Make any changes needed to script for video drivers, etc.
    - for nvidia, with multiple kernels, install `nvidia-dkms`
1. `./install-encrypt.sh`
1. `reboot`

## After reboot
1. `timedatectl set-ntp true`
1. `ansible-playbook <path-to-laptop>/systemd-user.yml`
1. `gpg --import matt-xyz-secret.asc`
1. `gpg --import-ownertrust matt-xyz-ownertrust.txt`
1. `ssh-add ~/.ssh/id_rsa`
1. reboot again
1. if using vagrant/libvirt, install `vagrant plugin install vagrant-libvirt`

### Setting Up Printer for Work
1. Make sure hplip and cups are installed
1. Enable and start `org.cups.cupsd.service` if if isn't already
1. Navigate to web: http://localhost:631/admin/
1. Authenticate as user (assuming user in in `sys` group. Otherwise, root)
1. Choose 'Add Printer'
1. Select from Local Printers: the 'HP Printer(HPLIP)'
1. Add printer 'Connection' via `socket://ip-for-printer`
1. provide printer info (Make/Model)
1. Set printer as default `lpoptions -d ITS-Black-White` (tab-complete actual
   name)
1. PRINT `lpr a-thing-to-kill-a-tree`

